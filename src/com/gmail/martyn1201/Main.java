package com.gmail.martyn1201;

import java.net.URL;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        /*
        вводим с клавиатуры id пользователя
         */
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter id:");
        int id = scanner.nextInt();


        URL url = JsonUtility.getSpecifiedUrl(id); //получаем URL для этого пользователя

        String userJson = JsonUtility.parseUrl(url, id); //считываем данные с сервера

        JsonUtility.printUserFullName(userJson, id); //выводим необходимую информацию в консоль
        System.exit(0);
    }
}
