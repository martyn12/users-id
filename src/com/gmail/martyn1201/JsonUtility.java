package com.gmail.martyn1201;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;



public class JsonUtility {

    private static final String USERS_URL = "https://reqres.in/api/users/";
    /*
     метод для получения URL для каждого отдельного пользвателя
     используя id скомого контакта
    */
    public static URL getSpecifiedUrl(int id) {
        try {
            return new URL(USERS_URL + id);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }
    /*
     метод для считывания JSON файла с сервера и преобразование его в строку
     при отсутсвии данных для считывания выводится сообщение об ошибке
     */

    public static String parseUrl(URL url, int id) {
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()))) {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        } catch (IOException e) {
            System.out.println("java users-id " + id + "\n" + "User not found!"); //
            System.exit(1);
        }
        return stringBuilder.toString();
    }

    /*
    метод, в котором из JSON строки достается имя и фамилия и выводится на печать
     */
    public static void printUserFullName(String userJson, int id) {
        try{
            JSONObject userData = (JSONObject) JSONValue.parseWithException(userJson);
            JSONObject data = (JSONObject) userData.get("data");
            System.out.println("java users-id " + id + "\n" + data.get("first_name") + " " + data.get("last_name"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}